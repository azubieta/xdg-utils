# Xdg Utils C++
Provides an implementation of the Free Desktop Standards to be used in C++ projects.
 
It's has a modular design in order to allow clients to use only the required functionalities.

Currently implemented specifications:
- [Desktop Entry 1.2](https://standards.freedesktop.org/desktop-entry-spec/1.2/)

